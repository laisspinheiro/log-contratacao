package com.itau.log.contratacao.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;
import com.itau.log.contratacao.models.LogContratacao;
import com.itau.log.contratacao.repositories.LogContratacaoRepository;

public class LogContratacaoReciever {
	@Autowired
	private LogContratacaoRepository logContratacaoRepository;
	
	private final String QUEUE_ID = "produtos.queue.lancamentofinanceiro";

	@JmsListener(destination = QUEUE_ID)
	public void receiveMessage(String msgJson) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			LogContratacao logMensagem;
			logMensagem = mapper.readValue(msgJson, LogContratacao.class);
			Optional<LogContratacao> optionalLogDb = logContratacaoRepository.findByUniqueId(logMensagem.getUniqueId());

			if (!optionalLogDb.isPresent()) {				
				LogContratacao logDb = optionalLogDb.get();
				  //incluir evento na log
					logDb.setAcao(logMensagem.getAcao());
					logDb.setContrato(logMensagem.getContrato());
					logDb.setCpf(logMensagem.getCpf());
					logDb.setData(logMensagem.getData());
					logDb.setDescricao(logMensagem.getDescricao());
					logDb.setMeta(logMensagem.getMeta());
					logDb.setObservacao(logMensagem.getObservacao());
					logDb.setSigla(logMensagem.getSigla());
					logDb.setStatus(logMensagem.getStatus());
					logDb.setTipoOperacao(logMensagem.getTipoOperacao());
					logDb.setValor(logMensagem.getValor());
					logContratacaoRepository.save(logDb);
					System.out.println("Gravado com sucesso");
				} else {
					System.out.println("Evento ja registrado na log " + logMensagem.getUniqueId());
				}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Erro na conversao da mensagem");
		}
	}


}
