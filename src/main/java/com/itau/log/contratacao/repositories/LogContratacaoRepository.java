package com.itau.log.contratacao.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.google.common.base.Optional;
import com.itau.log.contratacao.models.LogContratacao;

@Repository
public interface LogContratacaoRepository extends CrudRepository<LogContratacao, Long> {
	Optional<LogContratacao> findByUniqueId(UUID uniqueId);

}
