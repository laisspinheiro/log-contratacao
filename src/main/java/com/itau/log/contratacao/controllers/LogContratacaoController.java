package com.itau.log.contratacao.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.log.contratacao.models.LogContratacao;
import com.itau.log.contratacao.repositories.LogContratacaoRepository;

@RestController
@RequestMapping("/log")
public class LogContratacaoController {

	@Autowired
	private LogContratacaoRepository logContratacaoRepository;
	
	@GetMapping("")
	public ResponseEntity<Iterable<LogContratacao>> getAll() {
		return ResponseEntity.ok(logContratacaoRepository.findAll());
	}
}
